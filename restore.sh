#!/bin/sh
if [ "$#" -gt "0" ]; then
    cd $1

    N=1
    ls | while IFS="" read -r file;
    do
        NEW_NAME=$(echo "${file}" | cut -c 5-)
        mv "${file}" "$NEW_NAME" 
    done
else
    echo "argument didn't set"
fi
