#!/bin/sh
if [ "$#" -gt "0" ]; then
    cd $1

    N=1
    ls -S -1 | while IFS="" read -r file;
    do
        if [ "$N" -lt "10" ]; then
            mv "${file}" "00${N}_${file}"
        elif [ "$N" -lt "100" ]; then
            mv "${file}" "0${N}_${file}"
        else
            mv "${file}" "${N}_${file}"
        fi
          N=`expr $N + 1`
    done
else
    echo "argument didn't set"
fi
