Renaming script

The main.sh script is used to rename all files in the specified folder by their sizes. The bigger file will be 001_filename, second file by size will be 002_filename and so on.
You need to specify the folder you want to change as the first argument.

Also there is a script restore.sh, which is used to restore the folder to state before renaming (be careful: do not use this script before main one, because it will erase part of the names of your files).
You also need to specify folder as an argument.
